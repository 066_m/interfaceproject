public class TestAble {
    public static void main(String[] args) {
        BBat bbat = new BBat("Lucifer");
        bbat.fly();
        Plane plane = new Plane("Engine Number I");
        plane.fly();
        plane.run();
        DDog ddog = new DDog("Sommai");
        ddog.run();
        Car car = new Car("Engine Number III");
        car.run();

        Flyable[] flyables = {bbat, plane};
        for(Flyable f : flyables){
            f.fly();
        }

        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
            }
            f.fly();
        }


    Runable[] runables = {ddog, plane, car};
        for(Runable r : runables){
            r.run();
        }
    }
}
