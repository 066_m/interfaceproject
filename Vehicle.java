public abstract class Vehicle {
    private String engine;

    public Vehicle(String engine){
        this.engine = engine;
    }

    public abstract void startEngine();
    public abstract void stopEngine();
    public abstract void raiseSpeed();
    public abstract void applyBreak();
}
