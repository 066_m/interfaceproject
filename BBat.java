public class BBat extends PPoultry {
    private String nickname;

    public BBat(String nickname){
        super("Bat", 2);
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + nickname + " fly!!");
        
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickname + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickname + " walk");
        
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickname + " speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickname + " sleep");
        
    }
}
